use std::fs;
use intcode;

fn main() {
    let file_name = "/home/alex/Repos/adventofcode-2019/input/day-9.txt";
    let input = fs::read_to_string(file_name).expect("Unable to read file");
    let initial_memory:Vec<i64> = input.split(',').filter_map(|w| w.parse().ok()).collect();
    let mut prog = intcode::Computer::new(initial_memory.to_vec());

    prog.push_back_input_buffer(1);
    prog.run();
    println!("P1: {:?}", prog.get_output_val());
    
    prog.reset();
    prog.push_back_input_buffer(2);
    prog.run();
    println!("P1: {:?}", prog.get_output_val());
}
