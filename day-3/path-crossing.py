import os
import operator

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../input/day-3.txt')

with open(filename) as f:
    wire_paths = f.read().splitlines()

def manhattan(vec):
    return abs(vec[0]) + abs(vec[1])

dir_to_cart_map = {
    'U': (0, 1),  'D': (0, -1),
    'L': (-1, 0), 'R': (1, 0)}

# Returns a dict of (X,Y) -> length
# where length is the running total length of the wire 
def make_path_from_dir_list(dirs):
    result = {}
    curr_pos = (0,0)
    length = 0
    for dir in dirs.split(','):
        length_of_segment = int(dir[1:])
        curr_dir =  dir_to_cart_map[dir[0]]
        for _i in range(length_of_segment):
            length += 1
            curr_pos = tuple(map(operator.add, curr_pos,curr_dir))
            result.setdefault(curr_pos, length)
    return result

A = make_path_from_dir_list(wire_paths[0])
B = make_path_from_dir_list(wire_paths[1])
crossings = set(A) & set(B) #& gives you common elements

first_crossing = min(manhattan(p) for p in crossings)
min_total_step_to_intersection = min(A[point] + B[point] for point in crossings)

print("P1: %i" % (first_crossing))
print("P2: %i" % (min_total_step_to_intersection))