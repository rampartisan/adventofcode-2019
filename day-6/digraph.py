import os
import math
import networkx as nx

dir_name = os.path.dirname(__file__)
file_name = os.path.join(dir_name, '../input/day-6.txt')

with open(file_name) as f:
    in_map = f.read().splitlines()

oribt_map = [i.split(')') for i in in_map]
graph = nx.Graph()
graph.add_edges_from(oribt_map)

num_orbits = 0
for n in graph.nodes():
    num_orbits += nx.shortest_path_length(graph,'COM',n)

print("P1: %i" % (num_orbits))
print("P2: %i" % (nx.shortest_path_length(graph, 'SAN', 'YOU') - 2)) # -2 to account for orbiting objects rather than "SAN"/"YOU"