use std::fs;
use intcode;
extern crate permutohedron;
use permutohedron::Heap;

static NUM_AMP: usize = 5;

fn run_amps(amps: &mut Vec<intcode::Computer>, mut base_phases: Vec<i64>) -> i64 {
    let mut phases = Heap::new(&mut base_phases);
    let mut max = 0;
    while let Some(perm) = phases.next_permutation() {
        for i in 0..NUM_AMP {
                amps[i].reset();
                amps[i].push_back_input_buffer(perm[i]);
        }

        let mut out_val: Option<i64> = Some(0);
        while amps[NUM_AMP - 1].get_status() != intcode::IntCodeStatus::HALT {
           
            for i in 0..NUM_AMP {
                match out_val {
                    None => (),
                    Some(x) =>  {amps[i].push_back_input_buffer(x);}
                }                
                amps[i].run();
                out_val = amps[i].get_output_val();
            }
        }

        match out_val {
            None => (),
            Some(x) =>  {if x > max { max = x};}
        }
    }

   max
}

fn main() {

    let file_name = "/home/alex/Repos/adventofcode-2019/input/day-7.txt";
    let input = fs::read_to_string(file_name).expect("Unable to read file");
    let initial_memory:Vec<i64> = input.split(',').filter_map(|w| w.parse().ok()).collect();
    let mut amps: Vec<intcode::Computer> = Vec::new();
    for _i in 0..NUM_AMP {
        amps.push(intcode::Computer::new(initial_memory.to_vec()));
    }
    
    let mut start_phase: usize = 0; 
    let mut base_phases: Vec<i64> = ((start_phase as i64)..(start_phase + NUM_AMP) as i64).collect();
    println!("P1: {}", run_amps(&mut amps, base_phases));

    start_phase = 5;
    base_phases = ((start_phase as i64)..(start_phase + NUM_AMP) as i64).collect();
    println!("P2: {}", run_amps(&mut amps, base_phases));


    
}