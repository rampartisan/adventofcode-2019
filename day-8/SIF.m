m = 25;
n = 6;

raw = fileread('../input/day-8.txt');
rows = regexp(raw, sprintf('\\w{1,%d}', m), 'match')';
o = numel(rows) / n;
layers = zeros(o,n,m);
num_zeros = zeros(o,1);

for i = 1:o
  for j = 1:n
    layers(i,j,:) = cellfun(@str2double,num2cell(rows{((i - 1) * n) + j}));
  endfor
  num_zeros(i) = nnz(~layers(i,:,:));
endfor

[~,min_zero_layer_idx] = min(num_zeros);
num_twos = sum(sum(layers(min_zero_layer_idx,:,:) == 2));
num_ones = sum(sum(layers(min_zero_layer_idx,:,:) == 1));
p1 = num_ones * num_twos;
disp ("P1 :"), disp (p1)

enc_image = zeros(m,n);

for i = 1:m
  for j = 1:n
    for k = 1:o
      if layers(k,j,i) != 2
        enc_image(i,j) = layers(k,j,i);
        break
      endif
    endfor
  endfor
endfor

imshow(~enc_image')
