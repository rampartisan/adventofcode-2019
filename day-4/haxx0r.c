#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define RANGE_MAX 999999
#define MAX_NUM_DIGITS 6
#define CHECK_FOR_REPEATING_SEQUENCE // uncomment for p2

int temp_array[MAX_NUM_DIGITS];

// little endian.
int split_int_to_digit_array(int number, int* array)
{
    int num_digits = 0;
    do
    {
        array[num_digits++] = number % 10;
        number /= 10;
    } while(number > 0);
    return num_digits;
}

bool check_number_is_valid_password(int number)
{
    int num_digits = split_int_to_digit_array(number, temp_array);
    if(num_digits != 6)
        return false;

    int num_doubles = 0;
    for(int i = (num_digits - 1); i > 0; i--)
    {
        if(temp_array[i] > temp_array[i - 1])
        {
            return false;
        }

        if(temp_array[i] == temp_array[i - 1])
        {
#ifdef CHECK_FOR_REPEATING_SEQUENCE
            int before = (i + 1) > (num_digits - 1) ? -1 : temp_array[i + 1];
            int after = (i - 2) < 0 ? -1 : temp_array[i - 2];
            if(temp_array[i] != before && temp_array[i] != after)
            {
                ++num_doubles;

            }
#elif
            ++num_doubles;
#endif
        }
    }
    return (num_doubles > 0);
}

int main(int argc, char *argv[]) 
{
    if(argc != 3)
    {
        printf("Arguments must be RANGE_LOW RANGE_HIGH \n");
        return 1;
    }

    int rlow = atoi(argv[1]);
    int rhigh = atoi(argv[2]);
    rlow =  (rlow > RANGE_MAX) ? RANGE_MAX : rlow;
    rhigh = (rhigh > RANGE_MAX) ? RANGE_MAX : rhigh;

    if(rhigh <= rlow)
    {
        printf("RANGE_HIGH must be greater than RANGE_LOW \n");
        return 1; 
    }

    int num_valid_passwords = 0;
    for(int i = rlow; i <= rhigh; i++)
    {
        if(check_number_is_valid_password(i))
        {
            ++num_valid_passwords;
        }

    }

    printf("%i \n", num_valid_passwords);
}
