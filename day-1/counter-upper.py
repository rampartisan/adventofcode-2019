import math
import numpy
import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../input/day-1.txt')

with open(filename) as f:
    mass_list = f.read().splitlines()

def calc_fuel_for_mass(mass): return (mass // 3) - 2
tot_fuel_req = sum([calc_fuel_for_mass(int(mass)) for mass in mass_list])
print("P1: {}".format(tot_fuel_req))

tot_fuel_req = 0
for mass in mass_list:
    mod_fuel_req =  calc_fuel_for_mass(int(mass))
    while(mod_fuel_req > 0):
        tot_fuel_req += mod_fuel_req
        mod_fuel_req = calc_fuel_for_mass(mod_fuel_req)

print("P2: {}".format(tot_fuel_req))
