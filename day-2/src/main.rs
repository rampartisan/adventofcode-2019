use std::fs;
use intcode;

fn main() {

    let file_name = "../input/day-2.txt";
    let input = fs::read_to_string(file_name).expect("Unable to read file");
    let mut initial_memory:Vec<i64> = input.split(',').filter_map(|w| w.parse().ok()).collect();
    initial_memory[1] = 12;
    initial_memory[2] = 2;

    let mut prog = intcode::Computer::new(initial_memory.to_vec());
    prog.run();
    println!("P1: {}", prog.get_mem_val(0));

    let target_output = 19690720;
    let mut noun = -1;
    let mut verb = -1;
    'noun_loop: for nidx in 0..99
    {
        'verb_loop: for vidx in 0..99
        {
            prog.set_mem(initial_memory.to_vec());
            initial_memory[1] = nidx;
            initial_memory[2] = vidx;
            prog.reset();
            prog.run();
            if prog.get_mem_val(0) == target_output {
                noun = nidx;
                verb = vidx;
                break 'noun_loop;
            }
        }
    }

    println!("P2: {}", 100 * noun + verb);
}
