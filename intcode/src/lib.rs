use std::io;
use std::collections::VecDeque;

fn cl_integer_input() -> i64 {
    let mut input = String::new();
    println!("Please enter a single integer");
    io::stdin().read_line(&mut input)
        .ok()
        .expect("Couldn't read line");
    return input.trim_end().parse::<i64>().unwrap();
}

fn cl_integer_output(val: i64){
    println!("IntCode: output -> {}",val);
}

#[derive(Copy, Clone, PartialEq)]
pub enum IntCodeStatus {
    OK,
    ERROR,
    HALT,
    PAUSE,
}

pub struct Computer {
    ip: usize,
    base: i64,
    ram: Vec<i64>,
    rom: Vec<i64>,
    input_buffer: VecDeque<i64>,
    output_val: Option<i64>,
    verbose: bool,
    use_external_io: bool,
    status: IntCodeStatus,
}

impl Computer {

    pub fn new(mem: Vec<i64>) -> Computer {
        Computer {
            ip: 0,
            base: 0,
            ram: mem.to_vec(),
            rom: mem.to_vec(),
            verbose: false,
            use_external_io: false,
            input_buffer: VecDeque::new(),
            output_val: None,
            status: IntCodeStatus::OK,
        }
    }

    pub fn reset(&mut self) {
        self.ram = self.rom.to_vec();
        self.base = 0;
        self.status = IntCodeStatus::OK;
        self.output_val = None;
        self.clear_input_buffer();
        self.ip = 0;
    }

    pub fn set_use_external_io(&mut self,set: bool){
        self.use_external_io = set;
    }

    pub fn set_verbose(&mut self,set: bool) {
        self.verbose = set;
    }

    pub fn get_mem_val(&mut self, idx: usize) -> i64{
        self.ram[idx]
    }

    pub fn get_status(&mut self) -> IntCodeStatus {
        self.status
    }

    pub fn push_back_input_buffer(&mut self, val: i64){
        self.input_buffer.push_back(val);
    }

    pub fn clear_input_buffer(&mut self) {
        self.input_buffer.clear();
    }

    pub fn get_output_val(&self) -> Option<i64> {
        self.output_val
    }

    pub fn set_mem(&mut self, mem: Vec<i64>) {
        self.rom = mem;
    }

    pub fn run(&mut self) {
        self.output_val = None;
        'exec_loop: while self.ip < self.ram.len() {
            let opcode = self.ram[self.ip] % 100;
            match opcode {
                1 =>       self.add(),
                2 =>       self.mult(),
                3 =>       self.input(),
                4 =>       self.output(),
                5 =>       self.jump_if_true(),
                6 =>       self.jump_if_false(),
                7 =>       self.less_than(),
                8 =>       self.equals(),
                9 =>       self.modify_base(),
                99 =>      {self.status = IntCodeStatus::HALT;},
                _opcode => {println!("IntCode: unkown opcode {}", opcode); self.status = IntCodeStatus::ERROR;},
            }

            match self.status {
                IntCodeStatus::OK =>    {if self.verbose {println!("IntCode: program OK at instruction {} -- code {}", self.ip, self.ram[self.ip])}; continue},
                IntCodeStatus::PAUSE => {if self.verbose {println!("IntCode: program paused at instruction {} -- code {}", self.ip, self.ram[self.ip])};break 'exec_loop;},
                IntCodeStatus::ERROR => {if self.verbose {println!("IntCode: program error at instruction {} -- code {}", self.ip, self.ram[self.ip])};break 'exec_loop;},
                IntCodeStatus::HALT =>  {if self.verbose {println!("IntCode: program halted at instruction {} -- code {}", self.ip, self.ram[self.ip])};break 'exec_loop;},
            }
        }
    }

    fn get_addr(&mut self, addr: usize) -> &mut i64 {
        if addr >= self.ram.len() {
            self.ram.resize(addr + 1, 0);
        }
        &mut self.ram[addr]
    } 

    fn get_param(&mut self, param: u32) -> &mut i64 {
        let arg = self.ip + param as usize;
        let pos = self.ram[arg];
        let mode = (self.ram[self.ip] / 10_i64.pow(param + 1)) % 10;
        match mode {
            0 =>  self.get_addr(pos as usize),
            1 =>  &mut self.ram[arg],
            2 =>  self.get_addr((pos + self.base) as usize),
            _ =>  panic!(),   
        }
    }

    fn add(&mut self) {
        *self.get_param(3) = *self.get_param(1) + *self.get_param(2);
        self.ip += 4;
        self.status = IntCodeStatus::OK;
    }

    fn mult(&mut self) {
        *self.get_param(3) = *self.get_param(1) * *self.get_param(2);
        self.ip += 4;
        self.status = IntCodeStatus::OK;
    }

    fn jump_if_true(&mut self)  {
        if *self.get_param(1) != 0 {
            self.ip = *self.get_param(2) as usize;
        } else {
            self.ip += 3;
        }
        self.status = IntCodeStatus::OK;
    }

    fn jump_if_false(&mut self) {
        if *self.get_param(1) == 0 {
            self.ip = *self.get_param(2) as usize;
        } else {
            self.ip += 3;
        }
        self.status = IntCodeStatus::OK;
    }

    fn less_than(&mut self) {
        *self.get_param(3) = if *self.get_param(1) < *self.get_param(2) {1} else {0};
        self.ip += 4;
        self.status = IntCodeStatus::OK;
    }

    fn equals(&mut self) {
        *self.get_param(3) = if *self.get_param(1) == *self.get_param(2) {1} else {0};
        self.ip += 4;
        self.status = IntCodeStatus::OK;
    }

    fn modify_base(&mut self) {
        self.base += *self.get_param(1);
        self.ip += 2;
        self.status = IntCodeStatus::OK
    }

    fn input(&mut self) {
        if self.use_external_io {
            self.input_buffer.push_back(cl_integer_input());
        }
        if let Some(x) = self.input_buffer.pop_front() {
            *self.get_param(1) = x;
            self.ip += 2;
            self.status = IntCodeStatus::OK
        } else {
            self.status = IntCodeStatus::PAUSE;
        }
    }

    fn output(&mut self) {
        let val = *self.get_param(1);
        self.output_val = Some(val);
        if self.use_external_io {
            cl_integer_output(val);
        }
        self.ip += 2;
        self.status = IntCodeStatus::OK;
    }    
}